/*
 * global settings
 */
var lambdaFunctionUrl = "https://qg3weuapwd.execute-api.us-west-2.amazonaws.com/UrlListRankerStage/urllistrankerresource";
var rankResultsDivId = "rank_result";
var redditDivId = "stage";
var defaultWidgetWidth = 250;
var defaultNumColumns = 4;

/*
 * global widget arrays
 */
var responseWidgetArray = [];
var redditWidgetArray = [];

/*
 * Api call from text input field
 */
function sendUrltoLambda() {
    getResponseFromLambda();
}

/*
 * Api call for reddit feed
 */
function showRedditArticles() {
    getRedditWidgets();
}

/*
 * runs reddit articles through the url validator lambda
 */
function getRedditWidgets() {
    $.getJSON('https://www.reddit.com/r/news/.json', function (data) {
        $('#stage').html('<h1>Loading...</h1>');

        var getParams = '?requestUrls=[';
        var articles = data.data.children;
        for(var i = 0; i < articles.length; i++) {
            getParams += '"' + articles[i].data.domain + '"';
            if(i != articles.length-1) {
                getParams += ',';
            }
        }
        getParams += ']';
        var completeLambdaUrl = lambdaFunctionUrl + getParams;
        var xhttp = new XMLHttpRequest();
        xhttp.timeout = 20000; 
        xhttp.open("GET", completeLambdaUrl, true);
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                var obj = JSON.parse(xhttp.responseText);
                rank = obj;
                for(var i = 0; i < articles.length; i++) {
                    redditWidgetArray.push(new NewsArticle(articles[i].data.title, articles[i].data.url, articles[i].data.domain, rank[i]));
                }
                writeWidgets('<h1>Trending Reddit articles</h1>',redditWidgetArray, redditDivId);
            } 
        };
        xhttp.send();
    });
}

/*
 * Calls UrlListRank lambda with a list of urls.  The lambda responds with a list of ranks in the same order as the URLs.
 */
function getResponseFromLambda() {
    var requestedUrl = document.getElementById("urlname").value;
    var completeLambdaUrl = lambdaFunctionUrl + '?requestUrls=["' + requestedUrl + '"]';
    var xhttp = new XMLHttpRequest();
    xhttp.timeout = 20000; 
    xhttp.open("GET", completeLambdaUrl, true);
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            var obj = JSON.parse(xhttp.responseText);
            var rank = obj[0];
            responseWidgetArray.push(new NewsArticle(null, requestedUrl, requestedUrl, rank));
            writeWidgets('', responseWidgetArray, rankResultsDivId);
        } 
    };
    xhttp.send();
}


/**
 * writes widgets from widgetArray to the html div element with divId
 */
function writeWidgets(title, widgetArray, divId) {
    var htmlOutput = title + '<table><tr>';
    for(var i = 0; i < widgetArray.length; i++) {
        if(i % defaultNumColumns == 0) {
            htmlOutput += '</tr><tr>';
        }
        htmlOutput += '<td>' + widgetArray[i].widgetHtml + '</td>';
    }
    htmlOutput += '</tr></table><hr>';
    document.getElementById(divId).innerHTML = htmlOutput;
}

/**
 * News Article object conatins info about a news article and it's html representation
 */
function NewsArticle(in_title, in_url, in_source, in_rank) {
    this.title = in_title;
    this.url = in_url;
    this.source = in_source;
    this.rank = in_rank;
    var colorString = "lightgreen";
    if(this.rank < 70) {
        colorString = "lightyellow";
    }
    if(this.rank < 30) {
        colorString = "pink";
    }
    var titleText;
    if(this.title) {
        titleText = this.title;
    } else {
        titleText = this.url;
    }
    this.widgetHtml = '<table border=a1 bgcolor=' + colorString + ' width=' + defaultWidgetWidth + '><tr><td><a href="' + this.url + '">' +
            titleText + '</a><br>' +
            'Source: ' + this.source + '<br>' +
            'Reliability index: ' + this.rank + 
            '</td><td><button id="upvote">Up</button><button id="downvote">Down</button></td></tr></table>'; 
}

