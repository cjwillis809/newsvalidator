# README #

News Validator Chrome Extension

# Installation #

To install this extension in Google Chrome:

1. Visit chrome://extensions in your browser (or open up the Chrome menu by clicking the icon to the far right of the Omnibox:  The menu's icon is three horizontal bars. and select Extensions under the Tools menu to get to the same place).

2. Ensure that the Developer mode checkbox in the top right-hand corner is checked.

3. Click Load unpacked extensionů to pop up a file-selection dialog.

4. Navigate to the directory in which your extension files live, and select it.
